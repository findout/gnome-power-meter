/* exported init */

const GETTEXT_DOMAIN = 'gnome-power-meter';

const { GObject, St, Clutter, Gio } = imports.gi;
const ByteArray = imports.byteArray;
const ExtensionUtils = imports.misc.extensionUtils;
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const _ = ExtensionUtils.gettext;

// return file contents as a string
function getFileContents(path) {
    const file = Gio.File.new_for_path(path);
    const [ok, contents, etag] = file.load_contents(null);
    const stringResult = ByteArray.toString(contents);
    return stringResult;
}

const batPath = '/sys/class/power_supply/BAT0';

// calulate power and return as 1 decimal fixed number,
// positive if charging, negative if discharging
function getPower() {
    const volts = parseInt(getFileContents(batPath + '/voltage_now'));
    const amperes = parseInt(getFileContents(batPath + '/current_now'));
    const watts = (volts * amperes / 100000000000) / 10;
    const status = getFileContents(batPath + '/status').trim();
    const signChar = {Charging:'+', Discharging:'-'}[status] || '';
    return signChar + watts.toFixed(1) + 'W';
}

const Indicator = GObject.registerClass(
class Indicator extends PanelMenu.Button {
    _init() {
        super._init(0.0, _('Gnome power meter'));

        let label = new St.Label({text: '',
            y_align: Clutter.ActorAlign.CENTER});
        this.add_child(label);

        setInterval(() => {
            try {
                label.text = getPower();
            } catch(err) {
                lg(err);
            }
        }, 1000)
    }
});

class Extension {
    constructor(uuid) {
        this._uuid = uuid;

        ExtensionUtils.initTranslations(GETTEXT_DOMAIN);
    }

    enable() {
        this._indicator = new Indicator();
        Main.panel.addToStatusArea(this._uuid, this._indicator);
    }

    disable() {
        this._indicator.destroy();
        this._indicator = null;
    }
}

function init(meta) {
    return new Extension(meta.uuid);
}

function lg(...msgs) {
    print("xxxpnt", ...msgs);
}