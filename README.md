# Gnome Power Meter indicator applet

Displayes the charging/discharging power in real-time. Positive for charging, negative for discharging.

## Installation

Tested on Ubuntu 22.04.

1. git clone ...

2. mv gnome-power-meter ~/.local/share/gnome-shell/extensions/gnome-power-meter@rende.se

3. logout and login

4. gnome-extensions enable gnome-power-meter@rende.se

The power meter should now appear at the top of screen.